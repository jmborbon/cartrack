//
//  LoginUIFailTests.swift
//  cartrackUITests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest

class LoginUIFailTests: XCTestCase {
    
    override func setUp() {
        continueAfterFailure = false
    }

    func testLoginFail() {
        let app = XCUIApplication()
        app.launchArguments = ["launchLogin"]
        app.launch()
        
        let loginView = app.otherElements["loginView"]
        if loginView.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
        
        let usernameTextField = app.textFields["usernameTextField"]
        if usernameTextField.waitForExistence(timeout: 10) {
            usernameTextField.tap()
            usernameTextField.typeText("username")
        } else {
            XCTFail()
        }
        
        let passwordTextField = app.secureTextFields["passwordTextField"]
        if passwordTextField.waitForExistence(timeout: 10) {
            // Success
            passwordTextField.tap()
            passwordTextField.typeText("password")
        } else {
            XCTFail()
        }
        
        let loginButton = app.buttons["loginButton"]
        if loginButton.waitForExistence(timeout: 10) {
            // Success
            loginButton.tap()
        } else {
            XCTFail()
        }
        
        let userTableView = app.tables["userTableView"]
        if userTableView.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
        
        let logoutButton = app.navigationBars.buttons["logoutButton"]
        if logoutButton.waitForExistence(timeout: 10) {
            // Success
            logoutButton.tap()
        } else {
            XCTFail()
        }
        
        if usernameTextField.waitForExistence(timeout: 10) {
            usernameTextField.tap()
            usernameTextField.typeText("username")
        } else {
            XCTFail()
        }
        
        if passwordTextField.waitForExistence(timeout: 10) {
            // Success
            passwordTextField.tap()
            passwordTextField.typeText("wrong")
        } else {
            XCTFail()
        }
        
        if loginButton.waitForExistence(timeout: 10) {
            // Success
            loginButton.tap()
        } else {
            XCTFail()
        }
        
        let loginFailed = app.alerts["loginFailed"]
        if loginFailed.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
        
        let okButton = loginFailed.buttons["OK"]
        if okButton.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
    }
}
