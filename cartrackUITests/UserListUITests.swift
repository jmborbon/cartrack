//
//  UserListUITests.swift
//  cartrackUITests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest

class UserListUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
    }

    func testUserList() {
        let app = XCUIApplication()
        app.launchArguments = ["launchUserList"]
        app.launch()
        
        let userTableView = app.tables["userTableView"]
        if userTableView.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
        
        let firstCell = userTableView.cells.firstMatch
        if firstCell.waitForExistence(timeout: 10) {
            // Success
            firstCell.tap()
        } else {
            XCTFail()
        }
        
        addUIInterruptionMonitor(withDescription: "Allow Location") {
            (alert) -> Bool in
            alert.buttons.firstMatch.tap()
            return true
        }
        
        let scrollView = app.scrollViews["scrollView"]
        if scrollView.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
        
        let myLocationButton = app.buttons["myLocationButton"]
        if myLocationButton.waitForExistence(timeout: 10) {
            // Success
            myLocationButton.tap()
        } else {
            XCTFail()
        }
        
        let addressButton = app.buttons["addressButton"]
        if addressButton.waitForExistence(timeout: 10) {
            // Success
            addressButton.tap()
        } else {
            XCTFail()
        }
        
        let backButton = app.navigationBars.buttons.firstMatch
        if backButton.waitForExistence(timeout: 10) {
            // Success
            backButton.tap()
        } else {
            XCTFail()
        }
        
        if userTableView.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
    }
    
}
