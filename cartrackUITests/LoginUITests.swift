//
//  LoginUITests.swift
//  cartrackUITests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest

class LoginUITests: XCTestCase {
    
    override func setUp() {
        continueAfterFailure = false
    }

    func testLoginSuccess() {
        let app = XCUIApplication()
        app.launchArguments = ["launchLogin"]
        app.launch()
        
        let loginView = app.otherElements["loginView"]
        if loginView.waitForExistence(timeout: 10) {
            // Success
        } else {
            XCTFail()
        }
        
        let usernameTextField = app.textFields["usernameTextField"]
        if usernameTextField.waitForExistence(timeout: 10) {
            usernameTextField.tap()
            usernameTextField.typeText("username")
        } else {
            XCTFail()
        }
        
        let passwordTextField = app.secureTextFields["passwordTextField"]
        if passwordTextField.waitForExistence(timeout: 10) {
            // Success
            passwordTextField.tap()
            passwordTextField.typeText("password")
        } else {
            XCTFail()
        }
        
        let countryTextField = app.textFields["countryTextField"]
        if countryTextField.waitForExistence(timeout: 10) {
            // Success
            countryTextField.tap()
        } else {
            XCTFail()
        }
        
        let pickerView = app.pickers["pickerView"]
        if pickerView.waitForExistence(timeout: 10) {
            // Success
            app.pickerWheels.element.adjust(toPickerWheelValue: "Philippines")
        } else {
            XCTFail()
        }
        
        let countryPickerView = app.otherElements["countryPickerView"]
        if countryPickerView.waitForExistence(timeout: 10) {
            // Success
            countryPickerView.tap()
        } else {
            XCTFail()
        }
        
        let rememberButton = app.buttons["rememberButton"]
        if rememberButton.waitForExistence(timeout: 10) {
            // Success
            rememberButton.tap()
        } else {
            XCTFail()
        }
        
        let loginButton = app.buttons["loginButton"]
        if loginButton.waitForExistence(timeout: 10) {
            // Success
            loginButton.tap()
        } else {
            XCTFail()
        }
    }
}
