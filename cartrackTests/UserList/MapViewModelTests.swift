//
//  MapViewModelTests.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest
import CoreLocation

@testable import cartrack

class MapViewModelTests: XCTestCase {

    override func setUp() {
        
    }
    
    func testLocation() {
        let location = CLLocation(latitude: -1.3234, longitude: 1.3033)
        let viewModel = MapViewModel(address: "address", location: location)
        XCTAssertEqual(viewModel.address, "address")
        XCTAssertNotNil(viewModel.coordinate())
    }
    
}
