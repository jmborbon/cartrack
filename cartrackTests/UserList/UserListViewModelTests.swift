//
//  UserListViewModelTests.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest
import RxSwift

@testable import cartrack

class UserListViewModelTests: XCTestCase {

    var viewModel : UserListViewModel!
    var mockService : MockUserListService!
    private var selectedViewModel : UserViewModel?
    private let disposeBag = DisposeBag()
    
    override func setUp() {
        mockService = MockUserListService()
        viewModel = UserListViewModel(service: mockService)
    }
    
    func testFetchingAndSelect() {
        let selectExpectation = expectation(description: "select")
        viewModel.didSelectViewModel = { viewModel in
            XCTAssertEqual(viewModel.title, self.selectedViewModel?.title)
            selectExpectation.fulfill()
        }
        let fetchExpectation = expectation(description: "fetching")
        viewModel.fetchUserViewModels().subscribe(onNext: { (userViewModels) in
            fetchExpectation.fulfill()
            XCTAssertFalse(userViewModels.isEmpty)
            self.selectedViewModel = userViewModels.first
            self.viewModel.select(viewModel: self.selectedViewModel!)
        }, onError: { (error) in
            XCTFail(error.localizedDescription)
        }).disposed(by: disposeBag)
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testLogout() {
        let logoutExpectation = expectation(description: "logout")
        viewModel.didLogout = {
            logoutExpectation.fulfill()
        }
        viewModel.logout()
        waitForExpectations(timeout: 10.0, handler: nil)
    }
}
