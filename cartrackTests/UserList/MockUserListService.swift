//
//  MockUserListService.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import Foundation
import RxSwift

@testable import cartrack

class MockUserListService : UserListServiceProtocol {
    
    func fetchUsers() -> Observable<[User]> {
        return Observable.create { (observer) -> Disposable in
            guard let path = Bundle(for: type(of: self)).path(forResource: "users", ofType: "json") else {
                return Disposables.create { }
            }
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let users = try JSONDecoder().decode([User].self, from: data)
                observer.onNext(users)
                return Disposables.create { }
            } catch {
                observer.onError(error)
                return Disposables.create { }
            }
        }
    }
}
