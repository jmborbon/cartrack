//
//  CountryPickerViewModelTests.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest

@testable import cartrack

class CountryPickerViewModelTests: XCTestCase {

    var viewModel : CountryPickerViewModel!
    
    override func setUp() {
        viewModel = CountryPickerViewModel(country: "Singapore")
    }
    
    func testCountryPicker() {
        XCTAssert(viewModel.numberOfRows() > 0)
        XCTAssertNil(viewModel.titleForRow(row: -1))
        XCTAssertNil(viewModel.titleForRow(row: 10000))
        XCTAssertEqual(viewModel.country, "Singapore")
        let changeExpectation = expectation(description: "change")
        viewModel.didUpdateCountry = { country in
            XCTAssertEqual(country, "Philippines")
            changeExpectation.fulfill()
        }
        viewModel.country = "Philippines"
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testDismiss() {
        let dismissExpectation = expectation(description: "dismiss")
        viewModel.didDismiss = {
            dismissExpectation.fulfill()
        }
        viewModel.dismiss()
        waitForExpectations(timeout: 10.0, handler: nil)
    }
}

