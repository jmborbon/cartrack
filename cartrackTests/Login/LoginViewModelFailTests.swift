//
//  LoginViewModelFailTests.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest

@testable import cartrack

class LoginViewModelFailTests: XCTestCase {

    var viewModel : LoginViewModel!
    var mockService : MockAccountServiceFail!
    
    override func setUp() {
        mockService = MockAccountServiceFail()
        viewModel = LoginViewModel(service: mockService)
    }
    
    func testLoginFailed() {
        let failExpectation = expectation(description: "fail")
        viewModel.didLoginSuccessful = {
            XCTFail()
        }
        viewModel.didLoginFailed = {
            failExpectation.fulfill()
        }
        viewModel.login(username: "username", password: "password")
        waitForExpectations(timeout: 10.0, handler: nil)
    }
}

