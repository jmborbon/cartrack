//
//  LoginViewModelTests.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import XCTest

@testable import cartrack

class LoginViewModelTests: XCTestCase {

    var viewModel : LoginViewModel!
    var mockService : MockAccountService!
    
    override func setUp() {
        mockService = MockAccountService()
        viewModel = LoginViewModel(service: mockService)
    }
    
    func testLoginSuccess() {
        let successExpectation = expectation(description: "success")
        viewModel.didLoginSuccessful = {
            successExpectation.fulfill()
        }
        viewModel.didLoginFailed = {
            XCTFail()
        }
        viewModel.login(username: "username", password: "password")
        waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    func testCountry() {
        let launchExpectation = expectation(description: "launch")
        viewModel.didLaunchCountryPicker = { country in
            XCTAssertEqual(country, "Singapore")
            launchExpectation.fulfill()
            self.viewModel.country = "Philippines"
        }
        let updateExpectation = expectation(description: "update")
        viewModel.didUpdateCountry = { country in
            XCTAssertEqual(country, "Philippines")
            updateExpectation.fulfill()
        }
        viewModel.selectCountry()
        waitForExpectations(timeout: 10.0, handler: nil)
    }
}
