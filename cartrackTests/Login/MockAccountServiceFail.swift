//
//  MockAccountServiceFail.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import Foundation

@testable import cartrack

class MockAccountServiceFail : AccountServiceProtocol {
    
    func createAccount(account: Account, remember: Bool) -> Bool {
        return false
    }
    
    func loginAccount(account: Account, remember: Bool) -> Bool {
        return false
    }
    
    func loggedInAccount() -> Account? {
        return nil
    }
    
    func logout() {
        
    }
}


