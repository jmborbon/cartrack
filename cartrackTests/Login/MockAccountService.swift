//
//  MockAccountService.swift
//  cartrackTests
//
//  Created by Jhon Michael Borbon on 24/6/21.
//

import Foundation

@testable import cartrack

class MockAccountService : AccountServiceProtocol {
    
    func createAccount(account: Account, remember: Bool) -> Bool {
        return true
    }
    
    func loginAccount(account: Account, remember: Bool) -> Bool {
        return true
    }
    
    func loggedInAccount() -> Account? {
        return Account(username: "username", password: "password", country: "Singapore")
    }
    
    func logout() {
        
    }
}

