//
//  ViewFragment.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 23/6/21.
//

import UIKit

class ViewFragment {
    
    class func add(child: UIViewController, to parent: UIViewController, container: UIView) {
        child.willMove(toParent: parent)
        parent.addChild(child)
        container.addSubview(child.view)
        child.didMove(toParent: parent)
        child.view.frame = container.bounds
    }
    
    class func remove(child: UIViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
}
