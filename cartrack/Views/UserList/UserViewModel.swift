//
//  UserViewModel.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import Foundation
import CoreLocation

class UserViewModel {
    
    private let user : User
    
    init(user: User) {
        self.user = user
    }
    
    var title : String {
        return user.name
    }
    
    var address : String {
        return user.address.street + " " + user.address.suite + " " + user.address.city + " " + user.address.zipcode
    }
    
    var location : CLLocation? {
        guard let lat = CLLocationDegrees(user.address.geo.lat),
              let lng = CLLocationDegrees(user.address.geo.lng) else {
            return nil
        }
        return CLLocation(latitude: lat, longitude: lng)
    }
    
    func id() -> (String, String) {
        return ("ID", "\(user.id)")
    }
    
    func username() -> (String, String) {
        return ("Username", user.username)
    }
    
    func email() -> (String, String) {
        return ("Email", user.email)
    }
    
    func phone() -> (String, String) {
        return ("Phone", user.phone)
    }
    
    func website() -> (String, String) {
        return ("Website", user.website)
    }
    
    func company() -> (String, String) {
        return ("Company", user.company.name)
    }
    
    func catchphrase() -> (String, String) {
        return ("", "\"" + user.company.catchPhrase + "\"" + "\n" + user.company.bs)
    }
    
}
