//
//  UserListViewModel.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import Foundation
import RxSwift

class UserListViewModel {
    
    let title = "User List"
    
    var didLogout:(()->())?
    var didSelectViewModel:((UserViewModel)->())?
    
    private let service : UserListServiceProtocol
    
    init(service: UserListServiceProtocol = UserListService()) {
        // inject protocol for testability
        self.service = service
    }
    
    func fetchUserViewModels() -> Observable<[UserViewModel]> {
        // convert observable [User] to [UserViewModel]
        return service.fetchUsers().map { $0.map { UserViewModel(user: $0) } }
    }
    
    func select(viewModel: UserViewModel) {
        didSelectViewModel?(viewModel)
    }
    
    func logout() {
        didLogout?()
    }
}
