//
//  MapViewController.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 23/6/21.
//

import UIKit
import MapKit

class MapViewController : UIViewController {
    
    var viewModel : MapViewModel!
    
    private lazy var mapView: MKMapView = {
        let view = MKMapView()
        view.showsUserLocation = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        initialize()
    }
    
    private func setupUI() {
        self.view.addSubview(mapView)
        mapView.snp.makeConstraints { (make) in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
    }
    
    private func initialize() {
        guard let coordinate = viewModel.coordinate() else { return }
        
        // Pin
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = viewModel.address
        mapView.addAnnotation(annotation)
        
        // Camera
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.requestWhenInUserAuthorization()
        initialize()
    }
    
    func centerToLocation() {
        guard let coordinate = viewModel.coordinate() else { return }
        mapView.setCenter(coordinate, animated: true)
    }
    
    func centerToMyLocation() {
        mapView.setCenter(mapView.userLocation.coordinate, animated: true)
    }
}
