//
//  UserListService.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import Foundation
import RxSwift

protocol UserListServiceProtocol {
    
    func fetchUsers() -> Observable<[User]>
    
}

class UserListService : UserListServiceProtocol {
    
    func fetchUsers() -> Observable<[User]> {
        return Observable.create { (observer) -> Disposable in
            let url = URL(string: "https://jsonplaceholder.typicode.com/users")!
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let clientError = error {
                    // client error
                    observer.onError(clientError)
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse,
                    (200...299).contains(httpResponse.statusCode) else {
                    // server error
                    observer.onError(NSError(domain: "", code: -1, userInfo: nil))
                    return
                }
                
                if let jsonData = data {
                    do {
                        let users = try JSONDecoder().decode([User].self, from: jsonData)
                        observer.onNext(users)
                    } catch {
                        observer.onError(error)
                    }
                }
            }
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
}
