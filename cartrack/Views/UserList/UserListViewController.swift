//
//  UserListViewController.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import UIKit
import RxSwift
import RxCocoa

class UserListViewController : UIViewController {
    
    var viewModel : UserListViewModel!
    
    private let disposeBag = DisposeBag()
    
    private lazy var userTableView : UITableView = {
        let tableView = UITableView()
        tableView.accessibilityIdentifier = "userTableView"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        initialize()
    }
    
    private func setupUI() {
        self.title = viewModel.title
        self.view.backgroundColor = .white
        self.view.addSubview(userTableView)
        userTableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin)
        }
        let logoutButton = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(logout))
        logoutButton.accessibilityIdentifier = "logoutButton"
        self.navigationItem.rightBarButtonItem = logoutButton
    }

    private func initialize() {
        // Data source
        viewModel.fetchUserViewModels().bind(to: userTableView.rx.items(cellIdentifier: "cell")) { index, viewModel, cell in
            cell.textLabel?.text = viewModel.title
            cell.selectionStyle = .none
            cell.accessoryType = .disclosureIndicator
        }.disposed(by: disposeBag)
        
        // Delegate
        userTableView.rx.modelSelected(UserViewModel.self).subscribe(onNext: { viewModel in
            self.viewModel.select(viewModel: viewModel)
        }).disposed(by: disposeBag)
        
        userTableView.rx.itemSelected.subscribe(onNext: { indexPath in
            self.userTableView.deselectRow(at: indexPath, animated: true)
        }).disposed(by: disposeBag)
    }
    
    @objc private func logout() {
        viewModel.logout()
    }
}
