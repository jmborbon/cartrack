//
//  UserViewController.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 21/6/21.
//

import UIKit

class UserViewController : UIViewController {
    
    var viewModel : UserViewModel!
    var mapViewController: MapViewController!
    
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.accessibilityIdentifier = "scrollView"
        view.showsVerticalScrollIndicator = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var contentView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 10.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var mapContainerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10.0
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var myLocationButton : UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = "myLocationButton"
        button.setTitle("My Location", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .darkGray
        button.layer.cornerRadius = 10.0
        button.addTarget(self, action: #selector(focusMyLocation), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var addressButton : UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = "addressButton"
        button.setTitle("Address", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemRed
        button.layer.cornerRadius = 10.0
        button.addTarget(self, action: #selector(focusAddress), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        initialize()
    }
    
    private func setupUI() {
        self.title = viewModel.title
        self.view.backgroundColor = .white
        self.view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.topMargin).offset(20)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin).offset(-20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.leading.trailing.equalToSuperview()
            make.width.equalTo(scrollView.snp.width)
        }
        // Profile
        let (idTitle, idValue) = viewModel.id()
        contentView.addArrangedSubview(item(title: idTitle, value: idValue))
        let (usernameTitle, usernameValue) = viewModel.username()
        contentView.addArrangedSubview(item(title: usernameTitle, value: usernameValue))
        let (emailTitle, emailValue) = viewModel.email()
        contentView.addArrangedSubview(item(title: emailTitle, value: emailValue))
        let (phoneTitle, phoneValue) = viewModel.phone()
        contentView.addArrangedSubview(item(title: phoneTitle, value: phoneValue))
        let (websiteTitle, websiteValue) = viewModel.website()
        contentView.addArrangedSubview(item(title: websiteTitle, value: websiteValue))
        let (companyTitle, companyValue) = viewModel.company()
        contentView.addArrangedSubview(item(title: companyTitle, value: companyValue))
        let (cpTitle, cpValue) = viewModel.catchphrase()
        contentView.addArrangedSubview(item(title: cpTitle, value: cpValue))
        
        // Map
        contentView.addArrangedSubview(mapContainerView)
        mapContainerView.snp.makeConstraints { (make) in
            make.height.equalTo(240)
        }
        // Buttons
        let horizontalStack = UIStackView()
        horizontalStack.axis = .horizontal
        horizontalStack.spacing = 10.0
        horizontalStack.alignment = .fill
        horizontalStack.distribution = .fillEqually
        horizontalStack.addArrangedSubview(addressButton)
        horizontalStack.addArrangedSubview(myLocationButton)
        myLocationButton.snp.makeConstraints { (make) in
            make.height.equalTo(50)
        }
        addressButton.snp.makeConstraints { (make) in
            make.height.equalTo(50)
        }
        contentView.addArrangedSubview(horizontalStack)
    }
    
    private func initialize() {
        
    }
    
    private func item(title: String, value: String) -> UIStackView {
        let horizontalStack = UIStackView()
        horizontalStack.axis = .horizontal
        horizontalStack.spacing = 10.0
        horizontalStack.alignment = .fill
        horizontalStack.distribution = .fillEqually
        let titleLabel = UILabel()
        titleLabel.font = .boldSystemFont(ofSize: 19)
        titleLabel.textColor = .black
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = title
        let valueLabel = UILabel()
        valueLabel.font = .systemFont(ofSize: 17)
        valueLabel.textColor = .darkGray
        valueLabel.translatesAutoresizingMaskIntoConstraints = false
        valueLabel.numberOfLines = 0
        valueLabel.text = value
        horizontalStack.addArrangedSubview(titleLabel)
        horizontalStack.addArrangedSubview(valueLabel)
        return horizontalStack
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ViewFragment.add(child: mapViewController, to: self, container: mapContainerView)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ViewFragment.remove(child: mapViewController)
    }
    
    @objc func focusMyLocation() {
        mapViewController.centerToMyLocation()
    }
    
    @objc func focusAddress() {
        mapViewController.centerToLocation()
    }

}
