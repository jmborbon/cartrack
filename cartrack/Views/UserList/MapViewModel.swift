//
//  MapViewModel.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 23/6/21.
//

import CoreLocation

class MapViewModel : NSObject {
    
    let address : String
    private let location : CLLocation?
    private let locationManager = CLLocationManager()
    
    init(address: String, location: CLLocation?) {
        self.address = address
        self.location = location
        super.init()
        self.locationManager.delegate = self
    }
    
    deinit {
        self.locationManager.delegate = nil
    }
    
    func coordinate() -> CLLocationCoordinate2D? {
        return location?.coordinate
    }
    
    func requestWhenInUserAuthorization() {
        if #available(iOS 14.0, *) {
            if locationManager.authorizationStatus == CLAuthorizationStatus.notDetermined {
                locationManager.requestWhenInUseAuthorization()
            } else if locationManager.authorizationStatus == CLAuthorizationStatus.denied {
                // Need to turn on from settings
            } else {
                locationManager.startUpdatingLocation()
            }
        } else {
            // Fallback on earlier versions
            if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            } else if CLLocationManager.authorizationStatus() == .denied {
                // Need to turn on from settings
            } else {
                locationManager.startUpdatingLocation()
            }
        }
        
    }
}

extension MapViewModel : CLLocationManagerDelegate {
    
    @available(iOS 14.0, *)
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus != CLAuthorizationStatus.denied {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .denied {
            locationManager.startUpdatingLocation()
        }
    }
    
}
