//
//  CountryPickerViewController.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import UIKit

class CountryPickerViewController : UIViewController {

    var viewModel : CountryPickerViewModel!
    
    lazy var countryPickerView : UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.accessibilityIdentifier = "pickerView"
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = .white
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        return pickerView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.accessibilityIdentifier = "countryPickerView"
        setupUI()
        initialize()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        self.view.addSubview(countryPickerView)
        countryPickerView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottomMargin)
        }
    }
    
    private func initialize() {
        if let selectedRow = viewModel.selectedRow() {
            countryPickerView.selectRow(selectedRow, inComponent: 0, animated: false)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        viewModel.dismiss()
    }
}

extension CountryPickerViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.titleForRow(row: row)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        viewModel.select(row: row)
    }
}
