//
//  LoginViewModel.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 19/6/21.
//

import Foundation
import RxSwift

final class LoginViewModel {
    
    let title = "Login"
    
    var didUpdateCountry:((String)->())?
    var didLaunchCountryPicker:((String)->())?
    
    var didLoginSuccessful:(()->())?
    var didLoginFailed:(()->())?
    
    let usernameTextPublishSubject = PublishSubject<String>()
    let passwordTextPublishSubject = PublishSubject<String>()
    
    var country : String = "Singapore" {
        didSet {
            didUpdateCountry?(country)
        }
    }
    
    var remember : Bool = true
    
    private let service : AccountServiceProtocol
    
    init(service: AccountServiceProtocol) {
        self.service = service
    }
    
    func isValid() -> Observable<Bool> {
        return Observable.combineLatest(usernameTextPublishSubject.asObservable().startWith(""),
                                        passwordTextPublishSubject.asObservable().startWith("")).map { username, password in
                                            return username.count > 0 && password.count > 0
                                        }
    }
    
    func login(username: String, password: String) {
        if service.loginAccount(account: Account(username: username, password: password, country: country), remember: remember) {
            didLoginSuccessful?()
        } else {
            didLoginFailed?()
        }
    }
    
    func selectCountry() {
        didLaunchCountryPicker?(country)
    }
    
}
