//
//  AccountService.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 22/6/21.
//

import Foundation
import CoreData

protocol AccountServiceProtocol {
    
    func createAccount(account: Account, remember: Bool) -> Bool
    func loginAccount(account: Account, remember: Bool) -> Bool
    func loggedInAccount() -> Account?
    func logout()
    
}

class AccountService : AccountServiceProtocol {
    
    // MARK: - Public functions
    
    func createAccount(account: Account, remember : Bool) -> Bool {
        let context = persistentContainer.viewContext
        
        guard findAccount(username: account.username) == false else {
            print("Account already exists")
            return false
        }
        
        let accountDetails = NSEntityDescription.insertNewObject(forEntityName: "AccountDetails", into: context) as! AccountDetails
        
        accountDetails.username = account.username
        accountDetails.password = account.password
        accountDetails.country = account.country
        accountDetails.login = remember
        
        do {
            try context.save()
            return true
        } catch {
            print("Creation failed! \(error)")
        }
        
        return false
    }
    
    func loginAccount(account: Account, remember : Bool) -> Bool {
        let context = persistentContainer.viewContext
        
        if let accountDetails = fetchAccount(username: account.username, password: account.password) {
            accountDetails.country = account.country
            accountDetails.login = remember
            
            do {
                try context.save()
                return true
            } catch {
                print("Login failed! \(error)")
            }
        } else {
            print("Account not found!")
            return createAccount(account: account, remember: remember)
        }
        
        return false
    }
    
    func loggedInAccount() -> Account? {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<AccountDetails>(entityName: "AccountDetails")
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "login == true")
        
        do {
            let entities = try context.fetch(fetchRequest)
            if let accountDetails = entities.first, let username = accountDetails.username, let password = accountDetails.password, let country = accountDetails.country {
                return Account(username: username, password: password, country: country)
            }
        } catch {
            print("Fetch failed! \(error)")
        }
        
        return nil
    }
    
    func logout() {
        let context = persistentContainer.viewContext
        fetchAll()?.forEach { $0.login = false }
        do {
            try context.save()
        } catch {
            print("Logout failed! \(error)")
        }
    }
    
    // MARK: - Private functions
    
    private func findAccount(username: String) -> Bool {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<AccountDetails>(entityName: "AccountDetails")
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "username == %@", username)
        
        do {
            let entities = try context.fetch(fetchRequest)
            return !entities.isEmpty
        } catch {
            print("Find failed! \(error)")
        }
        
        return false
    }
    
    private func fetchAccount(username: String, password: String) -> AccountDetails? {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<AccountDetails>(entityName: "AccountDetails")
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "username == %@ && password == %@", username, password)
        
        do {
            let entities = try context.fetch(fetchRequest)
            return entities.first
        } catch {
            print("Fetch failed! \(error)")
        }
        
        return nil
    }
    
    private func fetchAll() -> [AccountDetails]? {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<AccountDetails>(entityName: "AccountDetails")
        
        do {
            let entities = try context.fetch(fetchRequest)
            return entities
        } catch {
            print("Fetch failed! \(error)")
        }
        
        return nil
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "AccountDataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
}
