//
//  LoginViewController.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 19/6/21.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class LoginViewController : UIViewController {
    
    var viewModel : LoginViewModel!
    
    private let disposeBag = DisposeBag()
    
    private lazy var loginView: UIStackView = {
        let stackView = UIStackView()
        stackView.accessibilityIdentifier = "loginView"
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 20.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var usernameTextField: UITextField = {
        let textField = UITextField()
        textField.accessibilityIdentifier = "usernameTextField"
        textField.borderStyle = .roundedRect
        textField.placeholder = "Username"
        textField.autocapitalizationType = .none
        textField.textContentType = .username
        textField.keyboardType = .default
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.accessibilityIdentifier = "passwordTextField"
        textField.borderStyle = .roundedRect
        textField.textContentType = .password
        textField.placeholder = "Password"
        textField.isSecureTextEntry = true
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private lazy var countryTextField: UITextField = {
        let textField = UITextField()
        textField.accessibilityIdentifier = "countryTextField"
        textField.borderStyle = .roundedRect
        textField.placeholder = "Select Country"
        textField.delegate = self
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = "loginButton"
        button.addTarget(self, action: #selector(login), for: .touchUpInside)
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .darkGray
        button.layer.cornerRadius = 10.0
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var rememberButton: UIButton = {
        let button = UIButton()
        button.accessibilityIdentifier = "rememberButton"
        button.addTarget(self, action: #selector(remember), for: .touchUpInside)
        button.setTitle("Remember Me", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setImage(#imageLiteral(resourceName: "img_free_select"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 20)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        initialize()
    }
    
    private func setupUI() {
        self.title = viewModel.title
        self.view.backgroundColor = .white
        self.view.addSubview(loginView)
        loginView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin).offset(20)
        }
        loginView.addArrangedSubview(usernameTextField)
        loginView.addArrangedSubview(passwordTextField)
        loginView.addArrangedSubview(countryTextField)
        loginView.addArrangedSubview(loginButton)
        loginView.addArrangedSubview(rememberButton)
        loginButton.snp.makeConstraints { (make) in
            make.height.equalTo(40)
        }
        
        // Bind UI to observable
        usernameTextField.rx.text.map { $0 ?? "" }.bind(to: viewModel.usernameTextPublishSubject).disposed(by: disposeBag)
        passwordTextField.rx.text.map { $0 ?? "" }.bind(to: viewModel.passwordTextPublishSubject).disposed(by: disposeBag)
        
        // Bind observable back UI
        viewModel.isValid().bind(to: loginButton.rx.isEnabled).disposed(by: disposeBag)
        viewModel.isValid().map { $0 ? 1.0 : 0.5 }.bind(to: loginButton.rx.alpha).disposed(by: disposeBag)
    }
    
    private func initialize() {
        viewModel.didUpdateCountry = { country in
            self.countryTextField.text = country
        }
        countryTextField.text = viewModel.country
    }
    
    @objc func login() {
        self.view.endEditing(true)
        guard let username = usernameTextField.text, let password = passwordTextField.text else { return }
        viewModel.login(username: username, password: password)
    }
    
    @objc func remember() {
        self.view.endEditing(true)
        if viewModel.remember {
            viewModel.remember = false
            rememberButton.setImage(#imageLiteral(resourceName: "img_free_unselect"), for: .normal)
        } else {
            viewModel.remember = true
            rememberButton.setImage(#imageLiteral(resourceName: "img_free_select"), for: .normal)
        }
    }
}

extension LoginViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        viewModel.selectCountry()
        return false
    }
}
