//
//  CountryPickerViewModel.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import Foundation

class CountryPickerViewModel {
    
    var didUpdateCountry:((String)->())?
    var didDismiss:(()->())?
    
    var country : String {
        didSet {
            didUpdateCountry?(country)
        }
    }
    
    private var countrySelection : [String] {
        return Locale.isoRegionCodes.compactMap { Locale.current.localizedString(forRegionCode: $0) }
    }
    
    init(country: String) {
        self.country = country
    }
    
    func selectedRow() -> Int? {
        return countrySelection.firstIndex(of: country)
    }
    
    func numberOfRows() -> Int {
        return countrySelection.count
    }
    
    func titleForRow(row: Int) -> String? {
        guard row >= 0 && row < countrySelection.count else { return nil }
        return countrySelection[row]
    }
    
    func select(row: Int) {
        guard let title = titleForRow(row: row) else { return }
        self.country = title
    }
    
    func dismiss() {
        didDismiss?()
    }
}
