//
//  Coordinator.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 19/6/21.
//

import Foundation

protocol  Coordinator : class {
    func start() -> Void
}
