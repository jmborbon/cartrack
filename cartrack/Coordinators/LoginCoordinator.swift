//
//  LoginCoordinator.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 19/6/21.
//

import UIKit

class LoginCoordinator : Coordinator {
    
    var didLoginSuccessful:(()->())?
    var didLoginFailed:(()->())?
    
    private let window: UIWindow
    private let service: AccountServiceProtocol
    
    private var loginViewModel : LoginViewModel?
    
    init(window: UIWindow, service: AccountServiceProtocol) {
        self.window = window
        self.service = service
    }
    
    func start() {
        let rootView = LoginViewController()
        loginViewModel = LoginViewModel(service: service)
        loginViewModel?.didLaunchCountryPicker = { country in
            self.launchCountryPicker(country)
        }
        loginViewModel?.didLoginSuccessful = {
            self.didLoginSuccessful?()
        }
        loginViewModel?.didLoginFailed = {
            self.didLoginFailed?()
        }
        rootView.viewModel = loginViewModel
        let navigationView = UINavigationController(rootViewController: rootView)
        navigationView.navigationBar.prefersLargeTitles = true
        window.rootViewController = navigationView
    }
    
    func launchCountryPicker(_ country: String) {
        let view = CountryPickerViewController()
        let viewModel = CountryPickerViewModel(country: country)
        viewModel.didUpdateCountry = { country in
            self.loginViewModel?.country = country
        }
        viewModel.didDismiss = {
            self.window.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
        }
        view.viewModel = viewModel
        view.modalPresentationStyle = .overCurrentContext
        view.modalTransitionStyle = .crossDissolve
        window.rootViewController?.present(view, animated: true, completion: nil)
    }
}
