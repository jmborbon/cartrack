//
//  AppCoordinator.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 19/6/21.
//

import UIKit

class AppCoordinator : Coordinator {
    
    private let window: UIWindow
    private let service : AccountServiceProtocol
    
    init(window: UIWindow, service: AccountServiceProtocol = AccountService()) {
        self.window = window
        self.service = service
    }
    
    func start() {
        #if DEBUG
        let arg = ProcessInfo.processInfo.arguments
        if arg.contains("launchLogin") {
            launchLogin()
            return
        } else if arg.contains("launchUserList") {
            launchUserList()
            return
        }
        #endif
        
        if let account = service.loggedInAccount() {
            print("\(account.username) is logged in!")
            launchUserList()
        } else {
            launchLogin()
        }
    }
    
    func launchLogin() {
        let loginCoordinator = LoginCoordinator(window: window, service: service)
        loginCoordinator.didLoginSuccessful = {
            self.launchUserList()
        }
        loginCoordinator.didLoginFailed = {
            self.launchLoginFailed()
        }
        loginCoordinator.start()
    }
    
    func launchUserList() {
        let userListCoordinator = UserListCoordinator(window: window)
        userListCoordinator.didLogout = {
            self.service.logout()
            self.launchLogin()
        }
        userListCoordinator.start()
    }
    
    func launchLoginFailed() {
        let alert = UIAlertController(title: "Incorrect username or password", message: nil, preferredStyle: .alert)
        alert.view.accessibilityIdentifier = "loginFailed"
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(ok)
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
