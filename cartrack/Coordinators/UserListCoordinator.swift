//
//  UserListCoordinator.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import UIKit
import CoreLocation

class UserListCoordinator : Coordinator {
    
    var didLogout:(()->())?
    
    private let window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let rootView = UserListViewController()
        let viewModel = UserListViewModel()
        viewModel.didSelectViewModel = { viewModel in
            self.launchUser(viewModel)
        }
        viewModel.didLogout = {
            self.didLogout?()
        }
        rootView.viewModel = viewModel
        let navigationView = UINavigationController(rootViewController: rootView)
        navigationView.navigationBar.prefersLargeTitles = true
        window.rootViewController = navigationView
    }
    
    
    func launchUser(_ viewModel: UserViewModel) {
        // User details
        let view = UserViewController()
        view.viewModel = viewModel
        
        // Map view
        let mapView = MapViewController()
        let mapViewModel = MapViewModel(address: viewModel.address, location: viewModel.location)
        mapView.viewModel = mapViewModel
        view.mapViewController = mapView
        
        if let navigationView = window.rootViewController as? UINavigationController {
            navigationView.pushViewController(view, animated: true)
        }
    }

}
