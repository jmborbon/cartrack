//
//  Account.swift
//  cartrack
//
//  Created by Jhon Michael Borbon on 20/6/21.
//

import Foundation

struct Account {
    let username : String
    let password : String
    let country : String
}
