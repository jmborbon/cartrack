# Cartrack Coding Challenge
submitted by Jhon Michael Felix Borbon


## Getting started
* Run ``pod install`` to setup workspace and dependencies
* Use the correct code signing and apple development team
* Project supports running on a minimum version of iOS 12

## Creating an account / Logging in
* For first time login, the App accepts any entered username and password as a sign up and stores it in the database
* For subsequent login, the App will validate the username and password from the database and will either proceed or prompt a failure


_Thank you for your time and the opportunity_
